# CS 351 - Exam 1 Study Guide

* Logistics
    * Scope: through and including V4
    * Closed book, closed notes.
    * Taken in class (physically present), on Kodiak
* Lexical Analysis
    * Recognizing tokens in a stream of characters.
    * Token has a name/ID and a lexeme.
    * A scanner/lexer converts a stream of characters to a stream of tokens.
    * In PLCC
        * Define tokens in PLCC using regex.
        * Longest match wins, ties broken by rule order.
        * skip "tokens" are not tokens, they are matched and thrown away.
        * Token names must be all caps.
        * Lexeme stored in Token's str attribute.
        * Use scan command to lex/scan a stream of characters and produce tokens.
* Syntactic Analysis
    * Organizing tokens into a deeper structure.
    * A parser converts a stream of tokens into a parse tree.
    * BNF - Backus-Naur Form
        * terms: terminals (tokens) and non-terminals
        * rules: &lt;non-terminal> ::= terms
        * Every non-terminal on the RHS must appear on the LHS of at least one other rule (to define it).
        * Two rules that define the same non-terminal is an "or". Example
            * &lt;a> ::= ONE \
&lt;a> ::= TWO
            * "&lt;a> is either a ONE or a TWO."
    * In PLCC
        * LHS of the first rule is the start symbol.
        * PLCC is an LL(1) parser
            * A recursive descent parser following a **leftmost derivation** of a BNF grammar.
            * **Looks ahead 1 token** to determine which syntactic rule to apply.
            * So grammars must be **predictive** and **unambiguous** for PLCC to parse them.
        * In the RHS, a &lt;TOKEN> enclosed in angle brackets will be captured as an attribute into the parse tree. A TOKEN that is not enclosed in angle brackets will not.
        * **= Repetition rule
        * A rule with an empty RHS means the LHS can be an empty string.
        * Multiple rules with the same LHS each need a unique class name
            * &lt;a>:One ::= ... \
&lt;a>:Two ::= ...
        * Multiple occurances of a capturing term (&lt;TOKEN> or &lt;non-terminal>) with the same name must be given a unique attribute name.
            * &lt;tree> ::= L &lt;tree>left COMMA &lt;tree>right R
        * Able to write Java class signatures for each rule.
            * Know that multiple rules with the same LHS non-terminal generates an abstract base class.
* Semantic Analysis
    * Manifesting the meaning of a sentence in a language.
    * In PLCC
        * Attach methods to parse tree nodes.
        * rep command
        * rep calls $run on the root of the tree (an instance of the start symbol's class). Override $run in start symbol's class to start the evaluation of your parse tree.
        * Makes heavy use of recursion (and mutual recursion) and polymorphism.
* V0-V4
    * Env and its family of classes
        * Binding
        * Bindings
        * Val, IntVal, and ProcVal
    * Expressions: their syntax and semantics
        * LitExp
        * VarExp
        * PrimAppExp
        * LetExp
        * IfExp
        * ProcExp
        * AppExp
    * Able to draw correct environment diagrams for expressions
    * Closures
    * Occurs free and occurs bound
    * Able to correctly evaluate expressions
    * Static vs Dynamic scoping
