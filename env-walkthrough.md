# Env Walkthrough

The slides present Env and its related classes from an implementation standpoint.
Here I present from the perspective of a programmer that is going to use it.
That is, as an ADT (abstract data type), or as an API (application programmer's
interface).

```java
// Val is an abstract base type from which
// we'll derive new types.
Val three = new IntVal(3);
Val hi = new StrVal("hi");

// A Binding maps a String id to a value.
Binding x = new Binding("x", three);
assert x.val == three
assert x.id.equals("x");

// Bindings is a collection of Binding objects.
Bindings bindings = new Bindings();
bindings.add(x);
bindings.add("y", hi);

// We can lookup a Binding by it's id.
assert bindings.lookup("x") == x;
assert bindings.lookup("non-existent") == null;

Env empty = new EnvNull();
Env A = empty.extendEnv(bindings);
assert A instanceof EnvNode;
assert A.bindings == bindings;
assert A.env == empty;

Val val = A.applyEnv("x");
assert val == three;

try {
    A.applyEnv("Nope");
} catch (PLCCException e) {
    // EnvNode will look for "Nope" in its Bindings.
    // When it doesn't find it, it tries a.env.applyEnv("Nope").
    // EnvNull always throws a PCCException when applyEnv is called on it.
}

Bindings moreBindings = new Bindings();
moreBindings.add("x", hi);

Env B = A.extendEnv(moreBindings);
assert B.applyEnv("x") == hi; // Found in B's bindings
assert A.applyEnv("x") == three; // A still has a binding for "x".
                                 // Just can't see it through B.
assert B.applyEnv("y") == hi; // Found in A's bindings.
try {
    A.applyEnv("Nope");
} catch (PLCCException e) {
    // still blows up.
}
```
