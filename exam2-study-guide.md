# CS 351 - Exam 2 Study Guide

* Logistics
    * Scope: through and including V5 through TYPE1
    * Closed book, closed notes.
    * Taken in class (physically present), on Kodiak
* Slides 3
    * V5
        * letrec
    * V6
        * define
    * LIST (from homework)
        * be able to code very small recursive procs to solve simple problems
        * examples
            * sum values in given list
            * remove first occurance of a value in a list
            * count the number of values in a list
            * count the number of a specified value in a list
            * count the number of even (or odd) numbers in a list
            * return the value of the i^th element in a list
* Slides 3a
    * SET
        * side effects
        * all variables now bound to a Ref.
        * Ref has deRef() and setRef(v)
        * pass-by-value - all formal paramters receive a new reference to
          the expressed value of actual parameters.
    * REF
        * variables are passed-by-reference; formals and actuals share a
            reference. Thus a proc can change the passed variable.
        * all other parameter expressions are the same as pass-by-value
    * NAME
        * variables are passed-by-reference
        * procs and literals are passed-by-value
        * expresions are passed-by-thunk; that is formals are bound to
            a ThunkRef that contains the unevaluated actual parameter
            (an expression), and captures the calling environment.
            Each time formal is referenced in proc, its ThunkRef is
            dereferenced (deRef) which evaluates its expression in the
            captured (calling) environment.
        * Note, if an expression is not referenced, it is not evaluated.
    * NEED
        * Same as name, but ThunkRefs memoize the resulting value after
            the first deRef, and then return the memoized value on subsequent
            deRefs. So a passed expression is evaluated at most once.
    * CICO (in homework)
        * variables are pass-by-copy-in-copy-out; inside the proc each
            formal is bound to an unshared reference; that reference
            can be modified; when the proc is done evaluating, the value
            in the reference of the formal variable is copied out into
            the reference of the passed variable.
    * Order of evaluation
        * parameter evaluation
        * letDecl and letrecDecl evaluation
    * Aliasing
        * two variables sharing the same reference
    * Be able to evaluate expressions under different call semantics.
* Slides 4
    * TYPE0
    * TYPE1
    * Be able to detect type errors and explain why.
    * Be able to provide the type of an expression.
    * Be able to write the defined type of a proc.
    * Be able to declare the types for a proc, given its use.
    * dynamic vs static type checking
    * strong vs weak type checking
    * type error
