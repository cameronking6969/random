# TYPE1 Implementation Sketch

Parallels evaluation structure.

```plantuml
@startuml
skinparam groupInheritance 2

abstract Env {
    extendEnv(Bindings)
    {abstract} applyEnv(var): Val
    {abstract} applyEnvRef(var): Ref
}
class NodeEnv extends Env
class NullEnv extends Env

abstract TypeEnv {
    extendTypeEnv(TypeBindings)
    {abstract} applyTypeEnv(var): Type
}
class NodeTypeEnv extends TypeEnv
class NullTypeEnv extends TypeEnv

@enduml
```

```plantuml
@startuml
skinparam groupInheritance 2

abstract Val
class IntVal extends Val
class ProcVal extends Val
class BoolVal extends Val


abstract Type
class IntType extends Type
class ProcType extends Type
class BoolType extends Type

@enduml
```

```plantuml
@startuml
skinparam groupInheritance 2

abstract Exp {
    {abstract} eval(Env): Val
    {abstract} evalType(TypeEnv): Type
}
class LitExp extends Exp
class VarExp extends Exp
class PrimAppExp extends Exp
class IfExp extends Exp
class LetExp extends Exp
class ProcExp extends Exp
class AppExp extends Exp

@enduml
```

```plantuml
@startuml
skinparam groupInheritance 2

abstract TypeExp {
    {abstract} toType(): Type
}
class ProcTypeExp extends TypeExp
class PrimTypeExp extends TypeExp
class BoolTypeExp extends PrimTypeExp
class IntTypeExp extends PrimTypeExp


@enduml
```


## Checking for Type Equality

```plantuml
@startuml
skinparam groupInheritance 2

abstract Type {
    {abstract} checkEquals(t:Type)
    checkIntType(IntType) {throw}
    checkBoolType(BoolType) {throw}
    checkProcType(ProcType) {throw}
}
class IntType extends Type {
    checkEquals(t:Type) {t.checkIntType(this)}
    checkIntType(t) {} // true
}
class BoolType extends Type {
    checkEquals(t:Type) {t.checkBoolType(this)}
    checkBoolType(t) {} // true
}
class ProcType extends Type {
    returnType
    paramTypeList
    checkEquals(t:Type) {t.checkProcType(this)}
    checkProcType(ProcType t) {see below}
}

@enduml
```

###  Example: dispatching when types missmatch.
```java
Type i = new IntType();
Type b = new BoolType();
i.checkEquals(b);
    // Dispatched to IntType.checkEquals(Type t) because i is an IntType
    // t.checkIntType(this)
        // Dispatched to Type.checkIntType(IntType) because t is a BoolType
        // throw new TypeException()
```

### Example: dispathing when types match.
```java
Type i1 = new IntType();
Type i2 = new IntType();
i1.checkEquals(i2);
    // Dispatched to IntType.checkEquals(Type t) because i1 is an IntType
    // t.checkIntType(this)
        // Dispatched to IntType.checkIntType(IntType) because t is a IntType
        // Returns without throwing an exception. Means types are equal.
```

## ProcType.checkProcType(ProcType t)

```java
public void checkProcType(ProcType t) {
    // first check the return types
    returnType.checkEquals(t.returnType);
    // then check the types of the formal parameters
    checkEqualTypes(paramTypeList, t.paramTypeList);
}

public static void checkEqualTypes(List<Type> t1List, List<Type> t2List) {
    if (t1List.size() != t2List.size())
        throw new PLCCException("Type error", "argument number mismatch");
    Iterator<Type> t1i = t1List.iterator();
    Iterator<Type> t2i = t2List.iterator();
    while (t1i.hasNext()) {
        Type t1 = t1i.next();
        Type t2 = t2i.next();
        t1.checkEquals(t2);
    }
}
```

## Exp.evalType(tenv:TypeEnv)

* LitExp:  `3`
    * Return IntType
* TrueExp: `true`
    * Return BoolType
* FalseExp: `false`
    * Return BoolType
* VarExp:  `x`
    * Lookup and return the type of var in current type environment.
* SeqExp: Bad example `{ +(true, 3), 4}`
    * In the current type environment, evaluate the type of each expression to check they are correct.
        (to catch problems as in the example).
    * Return the type of the last expression.
* IfExp: Bad example `if 3 then true else 4`
    * In the current type environment, evaluate the type of test and check that it is a BoolType.
    * In the current type environment, evaluate the types of then and else and check that they are the same type.
    * Return the either then's or else's type.
* LetExp
    * In the current type environment, evaluate types of each RHS and bind them to variables in LHS, and extend the current type environment.
    * In the extended type environment, evaluate the type of the body.
    * Return the body's type.
* LetRecExp
    * Similar to LetExp
* AppExp:  `. e (e1, e2, ..., en)`
    * In the current type environment, evaluate type of e yeiding a ProcType.
    * In the current type environment, evaluate types of actual parameters in the current type environment.
    * Check that formal types match ProcType's actual types.
    * Return the ProcType's return type.
* PrimAppExp
    * Each primitive has a predefined ProcType; e.g., + is [int,int=>int]; returned by toType().
    * Handled like an AppExp.
* ProcExp:  `proc(x:int):bool y`
    * Construct ProcType from declared parameter types and return type.
    * Extend the current type environment with formals (vars, types).
    * In the extended type environment, evaluate the type of the body.
    * Check that body's type is the same type as the proc's return type.
    * Return the proc's defined type.

> **Key point:** Notice procs are not recursively evaluated during type evaluate as they
would be during value evaluation. If it did, then the type system
would be dynamic instead of static.

## Problem with mutually recursive proces and `define`

```
define odd? =
proc(x:int):bool if zero?(x) then false else .even?(sub1(x))
```

The  body of odd? raises a type exception because .even? isn't define and so doesn't have a type.

So let's define even? first.

```
define even? =
proc(x:int):bool if zero?(x) then true else .odd?(sub1(x))
```

Same problem in reverse. odd? is not defined.

Solution... introduce `declare`

```
declare odd? : [int=>int]

% Now we can define even? using odd?'s type to pass type checking.
define even? =
proc(t:int):bool if zero?(t) then true else .odd?(sub1(t))

$ Now we define odd?. Its type must match its declared type!
define odd? =
proc(t:int):bool if zero?(t) then false else .even?(sub1(t))
```

